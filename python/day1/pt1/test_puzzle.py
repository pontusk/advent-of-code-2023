import unittest
import io
import os
import contextlib
import puzzle

with io.open(
    os.path.join(os.path.dirname(__file__), "..", "input"),
    "r",
    encoding="utf8"
) as f:
    data = f.read()


class TestScript(unittest.TestCase):
    def test_pass(self) -> None:
        test_answer = puzzle.solve("1abc2")
        self.assertEqual(12, test_answer)

        test_answer = puzzle.solve("pqr3stu8vwx")
        self.assertEqual(38, test_answer)

        test_answer = puzzle.solve("a1b2c3d4e5f")
        self.assertEqual(15, test_answer)

        test_answer = puzzle.solve("treb7uchet")
        self.assertEqual(77, test_answer)

        with open(os.devnull, "w") as devnull:
            with contextlib.redirect_stdout(devnull):
                answer = puzzle.solve(data)

        print(f"Final answer: {answer}\n")


if __name__ == "__main__":
    unittest.main()
