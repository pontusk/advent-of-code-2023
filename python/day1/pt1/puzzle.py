import re


def parse(data: str) -> list[str]:
    result: list[str] = []
    for line in data.split("\n"):
        if line:
            digits = "".join(re.findall(r"\d+", line))
            result.append(digits)
    return result


def solve(data: str) -> int:
    lines = parse(data)
    numbers: list[int] = []

    for line in lines:
        first = line[0]
        last = line[-1]
        number = int(float(first + last))
        numbers.append(number)

    return sum(numbers)
