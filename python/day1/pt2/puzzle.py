import re


def parse(data: str) -> list[list[str]]:
    result: list[list[str]] = []
    for line in data.split("\n"):
        line_result = []
        if line:
            while len(line):
                match = re.search(
                    r"[1-9]|one|two|three|four|five|six|seven|eight|nine", line)
                if match:
                    line_result.append(match.group())
                    line = line[match.start() + 1:]
                else:
                    break
            result.append(line_result)

    return result


lookup = {
    "one": "1",
    "two": "2",
    "three": "3",
    "four": "4",
    "five": "5",
    "six": "6",
    "seven": "7",
    "eight": "8",
    "nine": "9",
}


def solve(data: str) -> int:
    lines = parse(data)
    numbers: list[int] = []

    for line in lines:
        first = line[0]
        last = line[-1]

        if first in lookup:
            first = lookup[first]

        if last in lookup:
            last = lookup[last]

        number = int(float(first + last))
        numbers.append(number)

    return sum(numbers)
