import unittest
import io
import os
import contextlib
import puzzle

with io.open(
    os.path.join(os.path.dirname(__file__), "..", "input"),
    "r",
    encoding="utf8"
) as f:
    data = f.read()


class TestScript(unittest.TestCase):
    def test_pass(self) -> None:
        test_answer = puzzle.solve(
            """two1nine
eightwothree
abcone2threexyz
xtwone3four
4nineeightseven2
zoneight234
7pqrstsixteen""")
        self.assertEqual(281, test_answer)

        with open(os.devnull, "w") as devnull:
            with contextlib.redirect_stdout(devnull):
                answer = puzzle.solve(data)

        print(f"Final answer: {answer}\n")


if __name__ == "__main__":
    unittest.main()
