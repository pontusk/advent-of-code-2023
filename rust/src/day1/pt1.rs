use regex::Regex;

fn parse(input: &str) -> impl Iterator<Item = Vec<&str>> {
    let re = Regex::new(r"\d").unwrap();
    input.lines().map(move |line| {
        re.find_iter(line)
            .map(|m| m.as_str())
            .collect::<Vec<&str>>()
    })
}

fn solve(input: &str) -> i32 {
    let lines = parse(input);
    lines
        .map(|line| {
            format!(
                "{}{}",
                line.first().unwrap_or(&"0"),
                line.last().unwrap_or(&"0")
            )
            .parse::<i32>()
            .unwrap()
        })
        .sum()
}

#[cfg(test)]
mod tests {
    use std::{env, fs};

    use super::*;

    #[test]
    fn test_solve() {
        let test_input = {
            "\
1abc2
pqr3stu8vwx
a1b2c3d4e5f
treb7uchet"
        };
        assert_eq!(142, solve(test_input));

        let path = env::var("CARGO_MANIFEST_DIR").unwrap() + "/src/day1/input";
        let input = fs::read_to_string(&path);

        match input {
            Ok(input) => println!("part 1: {}", solve(&input)),
            Err(_) => println!("Failed to read file: {}", path),
        }
    }
}
